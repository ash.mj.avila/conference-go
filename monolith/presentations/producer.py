import pika
import json


def approval_producer(presentation_object):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    channel.basic_publish(
        exchange="",
        routing_key="presentation_approvals",
        body=json.dumps(
            {
                "presenter_name": presentation_object.presenter_name,
                "presenter_email": presentation_object.presenter_email,
                "title": presentation_object.title,
            }
        ),
    )
    connection.close()

def approval_rejections(presentation_object):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_rejections")
    channel.basic_publish(
        exchange="",
        routing_key="presentation_rejections",
        body=json.dumps(
            {
                "presenter_name": presentation_object.presenter_name,
                "presenter_email": presentation_object.presenter_email,
                "title": presentation_object.title,
            }
        ),
    )
    connection.close()

# exchange='publish message' is the queue we create
# exchange_type= initiates the type of echange and in this case, fanout
# exchange_decalare(exchange=publish message, exchange_type="fanout")


# exchange='publish message'
# routing_key''= publishes to every queue
# body=message = the message we want to publish
# channel.basic_publish(exchange="publish message", routing_key='', body=message)
