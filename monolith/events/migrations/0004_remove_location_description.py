# Generated by Django 4.0.3 on 2023-02-11 02:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_location_description_location_weather'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='location',
            name='description',
        ),
    ]
