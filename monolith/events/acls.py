import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization":PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}
        # this will fix it if pexels delete the location in the future



def get_weather_data(city, state):

    # Create the URL for the geocoding API with the city and state
    header = {"Authorization": OPEN_WEATHER_API_KEY}
    url = "http://api.openweathermap.org/geo/1.0/direct"

    params = {
        "q": city + "," + state.abbreviation + "," + "840",
        "appid": OPEN_WEATHER_API_KEY,
    }
    # Make the request
    response = requests.get(url, headers=header, params=params)
    # Parse the JSON response
    geo = json.loads(response.content)

    # Get the latitude and longitude from the response
    try:
        lat_dict = geo[0]
        lat = lat_dict["lat"]
        lon_dict = geo[0]
        lon = lon_dict["lon"]
    except:
        return {"weather_data": "None reported."}
    # Create the URL for the current weather API with the latitude
    url = "https://api.openweathermap.org/data/2.5/weather"
    #   and longitude
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY
    }
    # Make the request
    response = requests.get(url, headers=header, params=params)
    # Parse the JSON response
    weather = json.loads(response.content)

    try:
        return {"temp": weather["main"]["temp"],
            "description": weather["weather"][0]["description"],
        }
    except:
        return {"None": None}
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
