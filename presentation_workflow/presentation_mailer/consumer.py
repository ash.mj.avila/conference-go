import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        # ALL OF YOUR CODE THAT HANDLES READING FROM THE
        # QUEUES AND SENDING EMAILS

        def process_approvals(ch, method, properties, body):
            data = json.loads(body)
            presenter_name = data["presenter_name"]
            presenter_email = data["presenter_email"]
            title = data["title"]
            send_mail(
                "Your presentation has been accepted",
                f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted",
                "admin@confernce.go",
                [presenter_email],
                fail_silently=False,
            )

        def process_rejections(ch, method, properties, body):
            data = json.loads(body)
            presenter_name = data["presenter_name"]
            presenter_email = data["presenter_email"]
            title = data["title"]
            send_mail(
                "Your presentation has been rejected",
                f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted",
                "admin@confernce.go",
                [presenter_email],
                fail_silently=False,
            )

        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approvals,
            auto_ack=True,
        )
        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejections,
            auto_ack=True,
        )

        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)


# exchange='publish message' is the queue we create
# exchange_type= initiates the type of echange and in this case, fanout
# channel.exchange_decalare(exchange=publish message, exchange_type="fanout")


# exchange='publish message' binds it to this exchange
# queue=queue_name = set it to whatever the name of the queue was›››
# channel.queue_bind(exchange="publish message", queue=queue_name)

# need to create a "process_manage" function at the top of these functions to describe what message we want processed
